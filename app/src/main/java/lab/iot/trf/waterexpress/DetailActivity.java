package lab.iot.trf.waterexpress;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import lab.iot.trf.waterexpress.model.ScaleData;
import lab.iot.trf.waterexpress.service.IKnotServiceConnection;
import lab.iot.trf.waterexpress.service.KnotIntegrationService;
import lab.iot.trf.waterexpress.service.OnDataChangedListener;
import lab.iot.trf.waterexpress.service.ServiceBinder;
import lab.iot.trf.waterexpress.util.PreferenceUtil;
import lab.iot.trf.waterexpress.util.Util;

public class DetailActivity extends AppCompatActivity implements OnDataChangedListener, ServiceConnection {

    IKnotServiceConnection mKnotServiceConnection;
    private String mDataDetail;
    private List<ScaleData> mDeviceData;
    boolean alerted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        TextView Volume = findViewById(R.id.Volume);
        Volume.setText("Volume (L): ");
        TextView TimeText = findViewById(R.id.TimeText);
        TimeText.setText("Ultima Atualização: ");
        alerted = false;
        }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent it = new Intent(this, KnotIntegrationService.class);
        startService(it);
        bindService(it, this, BIND_AUTO_CREATE);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {

        mKnotServiceConnection = ((ServiceBinder) service).getService();
        mKnotServiceConnection.subscribe(getIntent().getStringExtra(Util.EXTRA_DEVICE_UUID), DetailActivity.this);

    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }

    @Override
    public void onDataChanged(List<ScaleData> deviceData) {

        if (deviceData != null && deviceData.size() > 0) {

            mDataDetail = String.valueOf(deviceData.get(0).getCurrentValue());
            String mTimeStamp = deviceData.get(0).getTimestamp();
            TextView DataDetailText = findViewById(R.id.DataDetailText);
            DataDetailText.setText(mDataDetail);
            TextView Timestamp = findViewById(R.id.TimeStamp);
            Timestamp.setText(mTimeStamp);
        }

        final int nivelMonitor = PreferenceUtil.getInstance().getLevelMonitor();
        final int maxLevel = 20;
        if ( deviceData.get(0).getCurrentValue() <= nivelMonitor ) {

            if (!alerted){
                Toast.makeText(DetailActivity.this, "Alerta de nivel! Lança a activity de encomenda.", Toast.LENGTH_LONG).show();
                alerted = true;
            }

        }

        if (deviceData.get(0).getCurrentValue() == maxLevel){
                alerted = false;
            }

    }


}
