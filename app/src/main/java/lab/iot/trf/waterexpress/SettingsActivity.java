package lab.iot.trf.waterexpress;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import lab.iot.trf.waterexpress.R;
import lab.iot.trf.waterexpress.util.PreferenceUtil;
import lab.iot.trf.waterexpress.util.Util;

public class SettingsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        final SeekBar levelAlert = (SeekBar) findViewById(R.id.levelAlert);

        final TextView txtAlert = (TextView) findViewById(R.id.txtLevelAlert);

        final EditText url = (EditText) findViewById(R.id.url);
        final EditText uuid = (EditText) findViewById(R.id.uuid);
        final EditText token = (EditText) findViewById(R.id.token);

        String sharedPreferenceUrl = PreferenceUtil.getInstance().getEndPoint();
        String sharedPreferenceUuid = PreferenceUtil.getInstance().getUuid();
        String sharedPreferenceToken = PreferenceUtil.getInstance().getToken();

        if (sharedPreferenceUrl != "") {
            url.setText(sharedPreferenceUrl);
        }

        if (sharedPreferenceUrl == "") {
            url.setText(Util.KNOT_URL);
        }

        if (sharedPreferenceUuid != "") {
            uuid.setText(sharedPreferenceUuid);
        }

        if (sharedPreferenceUuid == "") {
            uuid.setText(Util.BERTOLI_UUID);
        }

        if (sharedPreferenceToken != "") {
            token.setText(sharedPreferenceToken);
        }

        if (sharedPreferenceToken == "") {
            token.setText(Util.BERTOLI_TOKEN);
        }

        final Button btnSaveSettings = (Button) findViewById(R.id.btnSaveSettings);

        final Button btnCancel = (Button) findViewById(R.id.botaoCancelar);

        final int nivelMonitor = PreferenceUtil.getInstance().getLevelMonitor();

        //Log.d("NIVELMONITOR",String.valueOf(nivelMonitor));

        if ( nivelMonitor != 99 ) {
            levelAlert.setProgress(nivelMonitor);
        }

        txtAlert.setText(String.valueOf(levelAlert.getProgress()));

        levelAlert.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                txtAlert.setText(String.valueOf(progress));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btnSaveSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PreferenceUtil.getInstance().setEndPoint(String.valueOf(url.getText()));
                PreferenceUtil.getInstance().setUuid(String.valueOf(uuid.getText()));
                PreferenceUtil.getInstance().setToken(String.valueOf(token.getText()));
                PreferenceUtil.getInstance().setLevelMonitor(levelAlert.getProgress());

                //Toast.makeText(SettingsActivity.this, String.valueOf(levelAlert.getProgress()), Toast.LENGTH_SHORT).show();
                //Log.d("NIVELMONITOR",String.valueOf(levelAlert.getProgress()));

                Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
                startActivity(intent);

                finish();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }



}
