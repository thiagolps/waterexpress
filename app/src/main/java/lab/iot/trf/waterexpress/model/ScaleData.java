package lab.iot.trf.waterexpress.model;

import android.provider.ContactsContract;

import br.org.cesar.knot.lib.model.AbstractThingData;
import br.org.cesar.knot.lib.model.Data;

/**
 * Created by thiago on 07/03/18.
 */

public class ScaleData extends AbstractThingData {

    private String uuid;
    private int currentValue;



    public int getCurrentValue() {
        if(data != null){
            int i = ((Double) data.value).intValue();
            return i;
        } else {
            return 100;
        }
    }

    public String getSwitchUUID() {
        return source;
    }

    public void setSwitchUUID(String switchUUID) {
        this.uuid = switchUUID;
    }

    public void setCurrentValue(int currentValue) {
        this.currentValue = currentValue;
    }

    @Override
    public String toString() {
        String value = super.toString() + " switchUUID = " + getSwitchUUID() +
                " currentValue = " + getCurrentValue();
        return value;
    }

}
