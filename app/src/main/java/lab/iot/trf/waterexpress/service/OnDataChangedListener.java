package lab.iot.trf.waterexpress.service;

import java.util.List;

import lab.iot.trf.waterexpress.model.ScaleData;

/**
 * Created by diegosouza on 1/29/18.
 */

public interface OnDataChangedListener {

    public void onDataChanged (List<ScaleData> deviceData);
}
