
package lab.iot.trf.waterexpress.service;

/**
 * Created by diegosouza on 1/28/18.
 */

public interface IKnotServiceConnection {
    void subscribe(String deviceUUID, OnDataChangedListener listener);
    void unsubscribe();

}
