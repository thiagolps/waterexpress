package lab.iot.trf.waterexpress.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import lab.iot.trf.waterexpress.model.ScaleDevice;

/**
 * Created by diegosouza on 2/7/18.
 */

public class SwitchDevicesAdapter extends BaseAdapter {

    private Context mContext;
    private List<ScaleDevice> mDevices;

    public SwitchDevicesAdapter(Context c, List<ScaleDevice> devices){
        mContext = c;
        mDevices = devices;

    }

    @Override
    public int getCount() {
        if (mDevices != null && mDevices.size() > 0) {
            return mDevices.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (mDevices != null && mDevices.size() > 0) {
            return mDevices.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (mDevices != null && mDevices.size() > 0 && mDevices.get(position) != null)  {

            //return Long.parseLong(mDevices.get(position).getToken());
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView tv = new TextView(mContext);

        tv.setText(mDevices.get(position).getUuid());

        ViewGroup.LayoutParams param = tv.getLayoutParams();
        if (param != null )param.height = (ViewGroup.LayoutParams.WRAP_CONTENT);

        tv.setTextSize(25);



        return tv;
    }
}
