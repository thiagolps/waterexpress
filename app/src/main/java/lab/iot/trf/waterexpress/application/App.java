package lab.iot.trf.waterexpress.application;

import android.app.Application;
import android.content.Context;

/**
 * Created by thiago on 28/02/18.
 */

public class App extends Application {

    private static Context mContext = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }

    public static Context getContext() {
        return mContext;
    }
}
