package lab.iot.trf.waterexpress.util;

import android.os.CountDownTimer;

public final class PoolingTimer {

    private PoolingListener listener;

    private static final long TEN_SECONDS = 1000 * 10;

    private CountDownTimer countDownTimer;

    private long periodPooling;

    public PoolingTimer(long intervalTick, PoolingListener listener) {
        this.listener = listener;
        this.periodPooling = intervalTick;
        countDownTimer = new Task(TEN_SECONDS, intervalTick);
    }

    public PoolingTimer(PoolingListener listener) {
        this(TEN_SECONDS, listener);
    }

    /**
     * Init the pooling
     */
    public void startPooling() {
        if (countDownTimer != null) {
            countDownTimer.cancel();

            // create a new CounterDown and start it
            countDownTimer = getTime();
            countDownTimer.start();
            Logger.d("Pooling started");
        }
    }

    /**
     * Stop the pooling
     */
    public void stopPooling() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    private Task getTime() {
        return new Task(TEN_SECONDS, periodPooling);
    }


    private class Task extends CountDownTimer {

        public Task(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long l) {
            if (listener != null) {
                Logger.d("Pooling Ticked");
                listener.onPoolingFinished();
            }
        }

        @Override
        public void onFinish() {
            // infinite loop
            Logger.d("Pooling Restarted");
            start();
        }
    }

    /**
     * Listener used to retrieve the result of pooling
     */
    public interface PoolingListener {
        public void onPoolingFinished();
    }
}
