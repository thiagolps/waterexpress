package lab.iot.trf.waterexpress.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import lab.iot.trf.waterexpress.model.ScaleData;
import lab.iot.trf.waterexpress.model.ScaleDevice;

/**
 * Created by diegosouza on 2/7/18.
 */

public class SwitchDataAdapter extends BaseAdapter {

    private Context mContext;
    private List<ScaleData> mDevicesData;

    public SwitchDataAdapter(Context c, List<ScaleData> devices){
        mContext = c;
        mDevicesData = devices;

    }

    @Override
    public int getCount() {
        if (mDevicesData != null && mDevicesData.size() > 0) {
            return mDevicesData.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (mDevicesData != null && mDevicesData.size() > 0) {
            return mDevicesData.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (mDevicesData != null && mDevicesData.size() > 0 && mDevicesData.get(position) != null)  {

            //return Long.parseLong(mDevices.get(position).getToken());
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        TextView tv = new TextView(mContext);

        tv.setText(String.valueOf(mDevicesData.get(position).getCurrentValue()));

        ViewGroup.LayoutParams param = tv.getLayoutParams();
        if (param != null )param.height = (ViewGroup.LayoutParams.WRAP_CONTENT);

        tv.setTextSize(25);

        return tv;
    }
}
