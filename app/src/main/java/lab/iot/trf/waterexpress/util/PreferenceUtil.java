package lab.iot.trf.waterexpress.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import lab.iot.trf.waterexpress.R;
import lab.iot.trf.waterexpress.application.App;

/**
 * Created by thiago on 27/02/18.
 */

public class PreferenceUtil extends Application {
    private final static String sharedPreference_File_Name = "lab.iot.trf.waterexpress_preferences";
    private final static String KEY_ENDPOINT_URL = "ENDPOINT_URL";
    private final static String KEY_UUID = "UUID";
    private final static String KEY_TOKEN= "TOKEN";
    private final static String KEY_LEVEL_MONITOR = "LEVEL_MONITOR";

    private static Object lock = new Object();
    private static PreferenceUtil sInstance;

    private PreferenceUtil() {

    }

    public static PreferenceUtil getInstance() {
        synchronized (lock) {
            if (sInstance == null) {
                sInstance = new PreferenceUtil();
            }
        }
        return sInstance;
    }

    private SharedPreferences getPref() {
        final Context context = App.getContext();
        return context.getSharedPreferences(sharedPreference_File_Name,Context.MODE_PRIVATE);
    }

    public String getEndPoint() {
        return getPref().getString(KEY_ENDPOINT_URL, lab.iot.trf.waterexpress.util.Util.EMPTY_STRING);
    }

    public void setEndPoint(String keyEndpointUrl) {
        getPref().edit().putString(KEY_ENDPOINT_URL,keyEndpointUrl).apply();
    }

    public String getUuid() {
        return getPref().getString(KEY_UUID, lab.iot.trf.waterexpress.util.Util.EMPTY_STRING);
    }

    public void setUuid(String uuid) {
        getPref().edit().putString(KEY_UUID,uuid).apply();
    }

    public String getToken() {
        return getPref().getString(KEY_TOKEN, lab.iot.trf.waterexpress.util.Util.EMPTY_STRING);
    }

    public void setToken(String token) {
        getPref().edit().putString(KEY_TOKEN,token).apply();
    }

    public int getLevelMonitor() {
        return getPref().getInt(KEY_LEVEL_MONITOR, lab.iot.trf.waterexpress.util.Util.EMPTY_LEVEL_MONITOR);
    }

    public void setLevelMonitor(int levelMonitor) {
        getPref().edit().putInt(KEY_LEVEL_MONITOR,levelMonitor).apply();
    }
}
