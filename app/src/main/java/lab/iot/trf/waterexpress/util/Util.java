package lab.iot.trf.waterexpress.util;

/**
 * Created by thiago on 28/02/18.
 */

public final class Util {
    public static final String EMPTY_STRING = "";
    public static final int EMPTY_LEVEL_MONITOR = 99;
    private static final String IP_TO_PING = "8.8.8.8";
    public static final String EXTRA_DEVICE_UUID = "DEVICE_UUID";
    public static final String EXTRA_CURRENT_VALUE = "CURRENT VALUE";
    public static final String KNOT_URL = "http://knot-test.cesar.org.br:3000";
    public static final String DEFAULT_UUID = "a9cc3e68-abea-4541-972a-39cf32bd0000";
    public static final String DEFAULT_TOKEN = "097ee5c09e978c3ebc3841c362107de832d50664";
    public static final String BERTOLI_UUID = "35d90b39-2020-498c-80e7-491dc2f40000";
    public static final String BERTOLI_TOKEN = "5654d3dbee0df472112a41a97f9c013b1434fa47";
/*
UUID:
35d90b39-2020-498c-80e7-491dc2f40000

Token:

5654d3dbee0df472112a41a97f9c013b1434fa47

UUID do gateway:

ef267eac-c96d-4815-bbc3-f34f41980000
*/

}
