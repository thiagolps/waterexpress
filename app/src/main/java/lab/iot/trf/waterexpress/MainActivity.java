package lab.iot.trf.waterexpress;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import br.org.cesar.knot.lib.connection.FacadeConnection;
import br.org.cesar.knot.lib.exception.InvalidDeviceOwnerStateException;
import br.org.cesar.knot.lib.exception.KnotException;
import br.org.cesar.knot.lib.model.KnotList;
import lab.iot.trf.waterexpress.adapter.SwitchDevicesAdapter;
import lab.iot.trf.waterexpress.model.ScaleDevice;
import lab.iot.trf.waterexpress.util.PreferenceUtil;
import lab.iot.trf.waterexpress.util.Util;

public class MainActivity extends AppCompatActivity {

    List<ScaleDevice> mDevicesList = null;
    private ListView mListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new SyncData().execute();




    }

    private void initListView(){
        mListView = new ListView(this);

        mListView.setAdapter(new SwitchDevicesAdapter(this, mDevicesList));

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ScaleDevice device = (ScaleDevice) parent.getItemAtPosition(position);

                if (device != null) {
                    //Toast.makeText(MainActivity.this, device.toString(), Toast.LENGTH_LONG).show();
                    Intent it = new Intent(MainActivity.this, DetailActivity.class);
                    it.putExtra(Util.EXTRA_DEVICE_UUID, device.getUuid());
                    startActivity(it);
                }
            }
        });

        setContentView(mListView);

    }


    public class SyncData extends AsyncTask<Object, Object , List<ScaleDevice>> {
        @Override
        protected List<ScaleDevice> doInBackground(Object[] objects) {
            KnotList<ScaleDevice> list = new KnotList<>(ScaleDevice.class);
            try {

                FacadeConnection.getInstance().setupHttp(PreferenceUtil.getInstance().getEndPoint(), PreferenceUtil.getInstance().getUuid(),PreferenceUtil.getInstance().getToken());

                mDevicesList = FacadeConnection.getInstance().httpGetDeviceList(list);

                Log.d("Size = " , String.valueOf(mDevicesList.size()));
            } catch (KnotException e) {
                Log.d("KnotExecption",String.valueOf(e));
                e.printStackTrace();
            } catch (InvalidDeviceOwnerStateException e) {
                Log.d("OwnerStateException",String.valueOf(e));
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<ScaleDevice> list) {
            super.onPostExecute(list);
            initListView();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Settings");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case 0:
                startActivity(new Intent(MainActivity.this,SettingsActivity.class));
                break;
        }

        return super.onOptionsItemSelected(item);

    }
}
